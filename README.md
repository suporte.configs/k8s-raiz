# Kubernetes Raíz - LABORATÓRIO



## Apresentação

Este documento visa habilitá-lo para trabalhar com o orquestrador de containers Kubernetes através do bootstrap de um cluster Kubernetes de alta disponibilidade com criptografia de ponta a ponta entre componentes e autenticação RBAC.
O Kubernetes é uma ferramenta de orquestração de código aberto desenvolvida pelo Google para gerenciar micro serviços ou aplicativos em contêineres em um cluster distribuído (nós). O Kubernetes fornece infraestrutura altamente resiliente com tempo de inatividade zero, retrocesso (rollback) automático, dimensionamento e autocorreção de contêineres (que consistem em posicionamento automático, reinicialização automática, replicação automática e dimensionamento de contêineres com base no uso da CPU).

O objetivo principal do Kubernetes é ocultar a complexidade de gerenciar uma frota de contêineres fornecendo  APIs REST para as funcionalidades necessárias. O Kubernetes é portátil por natureza, o que significa que pode ser executado em várias plataformas de nuvem públicas ou privadas ou bare-metal.


## Público-alvo
O público-alvo deste tutorial é alguém que planeja oferecer suporte a um cluster Kubernetes de produção e deseja entender como tudo se encaixa.

## Detalhes do Cluster
Está documento orienta você na inicialização de um cluster Kubernetes altamente disponível com criptografia de ponta a ponta entre componentes e autenticação RBAC.

* [kubernetes](https://github.com/kubernetes/kubernetes) 1.18.1
* [containerd](https://github.com/containerd/containerd) 1.3.3
* [coredns](https://github.com/coredns/coredns) v1.6.3
* [cni](https://github.com/containernetworking/cni) v0.8.5
* [etcd](https://github.com/coreos/etcd) v3.4.7



## Referências bibliográficas

* [Site kubernetes](https://kubernetes.io/docs/)
* [Kubernetes The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way)
* [kubernetes-the-hard-way-lab](https://github.com/kelseyhightower/kubernetes-the-hard-way)
* [ETCD](https://etcd.io/)





## Sumário


* [Pré-Requisitos](docs/01-prerequisitos.md)
* [Provisionamento e recursos](docs/02-provisionamentorecursos.md)
* [Instalar CFSSL e Kubectl ](docs/03-installclients.md)
* [Gerar Certificados ](docs/04-gerandocertificados.md)
* [Gerar arquivos de configuração ](docs/05-arquivosdeconfiguracao.md)
* [Gerar encriptação de dados de configuração e chave ](docs/06-arquivochaveencriptada.md)
* [Instalar Cluster ETCD ](docs/07-instalarclusteretcd.md)
* [Instalar Kubernetes Controll Plane ](docs/08-instalark8scontrollplane.md)
* [Load Balancer para o kubernetes ](docs/09-loadbalancer.md)
* [Instalar Kubernetes nos Works nodes ](docs/10-installarworknodes.md)
* [Configurar kubectl para acesso remoto ](docs/11-configkubectl.md)
* [Deploy do serviço de DNS ](docs/12-deploydns.md)
* [Teste cluster ](docs/13-testecluster.md)





