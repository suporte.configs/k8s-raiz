#!/bin/bash

curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
sudo python get-pip.py
pip install ansible==2.9.6

cp -Rf /vagrant/files/roles/* /etc/ansible/roles/
cp -Rf /vagrant/files/group_vars/* /etc/ansible/group_vars/

sudo ansible-playbook /vagrant/files/k8s_prepara_Maquinas.yml

mkdir -p /root/.ssh
cp /vagrant/files/id_rsa* /root/.ssh
chmod 400 /root/.ssh/id_rsa*
cp /vagrant/files/id_rsa.pub /root/.ssh/authorized_keys

HOSTS=$(head -n7 /etc/hosts)
echo -e "$HOSTS" > /etc/hosts
echo '172.27.11.100 admin-lb.k8s.hard.com admin-lb storage.k8s.hard.com storage' >> /etc/hosts
echo '172.27.11.11 master1.k8s.hard.com master1' >> /etc/hosts
echo '172.27.11.12 master2.k8s.hard.com master2' >> /etc/hosts
echo '172.27.11.13 master3.k8s.hard.com master3' >> /etc/hosts
echo '172.27.11.21 node1.k8s.hard.com node1' >> /etc/hosts
echo '172.27.11.22 node2.k8s.hard.com node2' >> /etc/hosts
echo '172.27.11.23 node3.k8s.hard.com node3' >> /etc/hosts

if [ "$HOSTNAME" == "storage" ]; then
        exit
fi

#cp /vagrant/files/selinux_config /etc/selinux/config
#setenforce 0
#sed -Ei 's/(.*swap.*)/#\1/g' /etc/fstab
#swapoff -a

ssh  root@172.27.11.100 'systemctl enable haproxy.service'
ssh  root@172.27.11.100 'systemctl enable nfs-server.service'

sudo reboot

