#!/bin/bash

sudo yum install -y nfs-utils haproxy ansible

sudo cp /vagrant/files/selinux_config /etc/selinux/config
sudo setenforce 0


sudo bash /vagrant/provision/packages.sh


sudo mkdir /nfsshare
sudo chmod -R 755 /nfsshare
sudo chown nfsnobody:nfsnobody /nfsshare

sudo systemctl start rpcbind
sudo systemctl start nfs-server
sudo systemctl start nfs-lock
sudo systemctl start nfs-idmap

sudo echo '/nfsshare    172.27.11.0/24(rw,sync,no_root_squash,no_all_squash)' >> /etc/exports

systemctl start nfs-server

sudo systemctl disable firewalld
sudo systemctl stop firewalld

sudo yum install iptables-services -y 
sudo systemctl stop iptables
sudo systemctl disable iptables


sudo cp /vagrant/files/haproxy.cfg /etc/haproxy/haproxy.cfg 
#sudo systemctl start haproxy
sudo cp /vagrant/files/rsyslog.conf /etc/rsyslog.conf 
#sudo systemctl restart haproxy
sudo cp /vagrant/files/haproxy.conf /etc/rsyslog.d/haproxy.conf 
sudo systemctl restart rsyslog 
sudo systemctl start haproxy

#sudo bash /vagrant/files/enableServices.sh


sudo mkdir -p /root/.ssh
sudo cp /vagrant/files/id_rsa* /root/.ssh
sudo chmod 400 /root/.ssh/id_rsa*
sudo cp /vagrant/files/id_rsa.pub /root/.ssh/authorized_keys

#sudo HOSTS=$(head -n7 /etc/hosts)
#sudo echo -e "$HOSTS" > /etc/hosts
#sudo echo '172.27.11.100 admin-lb.k8s.hard.com admin-lb' >> /etc/hosts
#sudo echo '172.27.11.11 master1.k8s.hard.com master1' >> /etc/hosts
#sudo echo '172.27.11.12 master2.k8s.hard.com master2' >> /etc/hosts
#sudo echo '172.27.11.13 master3.k8s.hard.com master3' >> /etc/hosts
#sudo echo '172.27.11.21 node1.k8s.hard.com node1' >> /etc/hosts
#sudo echo '172.27.11.22 node2.k8s.hard.com node2' >> /etc/hosts
#sudo echo '172.27.11.23 node3.k8s.hard.com node3' >> /etc/hosts

#if [ "$HOSTNAME" == "storage" ]; then
#        exit
#fi

sudo reboot



