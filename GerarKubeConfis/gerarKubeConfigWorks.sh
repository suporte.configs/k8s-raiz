#!/bin/bash

KUBERNETES_PUBLIC_ADDRESS="172.27.11.100"

for instance in node1.k8s.hard.com node2.k8s.hard.com  node3.k8s.hard.com; do
  kubectl config set-cluster kubernetes-unicit \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=${instance}.kubeconfig

   kubectl config set-credentials system:node:${instance} \
    --client-certificate=../GeraCertificados/${instance}.pem \
    --client-key=../GeraCertificados/${instance}-key.pem \
    --embed-certs=true \
    --kubeconfig=${instance}.kubeconfig

   kubectl config set-context default \
    --cluster=kubernetes-unicit \
    --user=system:node:${instance} \
    --kubeconfig=${instance}.kubeconfig

   kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done
