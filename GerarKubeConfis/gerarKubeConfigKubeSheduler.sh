#!/bin/bash

kubectl config set-cluster kubernetes-unicit \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-scheduler.kubeconfig

kubectl config set-credentials system:kube-scheduler \
    --client-certificate=../GeraCertificados/kube-scheduler.pem \
    --client-key=../GeraCertificados/kube-scheduler-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-scheduler.kubeconfig

kubectl config set-context default \
    --cluster=kubernetes-unicit \
    --user=system:kube-scheduler \
    --kubeconfig=kube-scheduler.kubeconfig

  
kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig
