#!/bin/bash
for instance in master1 master2 master3; do
  echo "Checar arquivos de configuração do kubernetes do ${instance}"
  ssh -o stricthostkeychecking=no root@${instance} ls -ltr /home/vagrant/*.kubeconfig
  echo "############################################################"
done

for instance in node1 node2 node3; do
  echo "Checar arquivos de configuração do kubernetes do ${instance}"
  ssh -o stricthostkeychecking=no root@${instance} ls -ltr /home/vagrant/*.kubeconfig
done
