#!/bin/bash

KUBERNETES_PUBLIC_ADDRESS="172.27.11.100"

kubectl config set-cluster kubernetes-unicit \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config set-credentials system:kube-proxy \
    --client-certificate=../GeraCertificados/kube-proxy.pem \
    --client-key=../GeraCertificados/kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config set-context default \
    --cluster=kubernetes-unicit \
    --user=system:kube-proxy \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
