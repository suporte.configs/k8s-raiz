#!/bin/bash

for instance in master1 master2 master3; do
  scp -C encryption-config.yaml root@${instance}:/home/vagrant/
  ssh root@${instance} chown vagrant:vagrant /home/vagrant/*
  ssh root@${instance} ls -ltr /home/vagrant/encryption-config.yaml
done

