#!/bin/bash


for instance in node1.k8s.hard.com node2.k8s.hard.com node3.k8s.hard.com; do
  scp -C ${instance}.kubeconfig kube-proxy.kubeconfig root@${instance}:/home/vagrant/
  ssh root@${instance} chown vagrant:vagrant /home/vagrant/*
done

for instance in master1 master2 master3; do
  scp -C admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig root@${instance}:/home/vagrant/
  ssh root@${instance} chown vagrant:vagrant /home/vagrant/*
done
