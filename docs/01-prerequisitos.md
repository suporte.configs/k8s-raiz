## Laboratório

Para compreeder o conteúdo deste documento é necessário no mínimo 4 maquinas virtuais do tipo GNU/Linux com comunicação entre si. No exemplo que se segue vamos utilizar o Vagrant para subir 3 VMs como master/controllers, 3 VMs como works e 1 VM como load balancer.

As mquinas virtuais devem possuir no minino 2GB de memória e 1vCPUs para as maquinas works e 4GG de memória e 2vCPUs para as maquinas masters/controllers.

| Nome  |  Endereço IP  |  Role  |
| ------------------- | ------------------- |  ------------------- |
|  storage.k8s.hard.com  |  172.27.11.100 |  Storage - NFS |
|  admin-lb.k8s.com |  172.27.11.100 |  HaProxy - Admin |
|  master1.k8s.com |  172.27.11.11 |  Controller |
|  master2.k8s.com |  172.27.11.12 |  Controller |
|  master3.k8s.com |  172.27.11.13 |  Controller |
|  node1.k8s.com |  172.27.11.21 |  Work |
|  node2.k8s.com |  172.27.11.22 |  Work |
|  node3.k8s.com |  172.27.11.23 |  Work |

-

| Network  |  Descrição  |
| ------------------- | ------------------- |  
|  Rede VirtualBox  |  LAN (externally reachable) | 
|  10.98.100.0/22  |  k8s Pod network | 
|  10.98.96.0/24  |  k8s Service network | 
|  172.27.11.100  |  k8s API server (external IP - haproxy) | 
|  172.27.11.*  |  ip hosts (internal IP) | 
|  172.27.11.1  |  k8s.com | 

# Na maquina Host
Adicionar ao arquivo /etc/hosts:

```
172.27.11.100 admin-lb.k8s.hard.com admin-lb storage.k8s.hard.com storage
172.27.11.11 master1.k8s.hard.com master1
172.27.11.12 master2.k8s.hard.com master2
172.27.11.13 master3.k8s.hard.com master3
172.27.11.21 node1.k8s.hard.com node1
172.27.11.22 node2.k8s.hard.com node2
172.27.11.23 node3.k8s.hard.com node3
```


