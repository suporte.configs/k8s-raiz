# Deploying do serviço de DNS no Cluster

Neste lab iremos atuar com o add-on do serviço de discovery de objetos em um DNS interno, utilizando o CoreDNS, para as aplicações que rodam dentro do Kubernetes.

## Cluster DNS Add-on

## Instalar resolvconf
```
$ yum install -y systemd-resolved
```

Obs: Todas as máquinas

## Instalar o coreDNS cluster(executar no remote kubectl):

```
$ kubectl apply -f https://storage.googleapis.com/kubernetes-the-hard-way/coredns.yaml
```

## Listar os pods criados no deployment kube-dns:

```
$ kubectl get pods -l k8s-app=kube-dns -n kube-system

NAME                       READY   STATUS    RESTARTS   AGE
coredns-699f8ddd77-94qv9   1/1     Running   0          20s
coredns-699f8ddd77-gtcgb   1/1     Running   0          20s
```

Referência troubleshooting containerd (crictl):
https://kubernetes.io/docs/tasks/debug-application-cluster/crictl/

## Verificação:
Criar um deployment de um container pequeno (busybox) para validar o DNS:
```
$ kubectl run busybox --image=busybox:1.28 --command -- sleep 3600

```
Listar o pod criado:
```
$ kubectl get pods -l run=busybox

NAME                      READY   STATUS    RESTARTS   AGE
busybox-bd8fb7cbd-vflm9   1/1     Running   0          10s
```

Pegar o nome completo do pod do container busybox:
```
$ POD_NAME=$(kubectl get pods -l run=busybox -o jsonpath="{.items[0].metadata.name}")
```
Executar o DNS lookup (nslookup) para o serviço do Kubernetes referente ao pod criado busybox. Dentro do pod busybox:
```
$ kubectl exec -ti $POD_NAME -- nslookup kubernetes

Server:    10.32.0.10
Address 1: 10.32.0.10 kube-dns.kube-system.svc.cluster.local



Name:      kubernetes
Address 1: 10.32.0.1 kubernetes.default.svc.cluster.local

```

