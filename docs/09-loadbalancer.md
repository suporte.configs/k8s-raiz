# Frontend Load Balancer para o Kubernetes

Neste lab já está provisionado a maquina "admin-lb" com o haproxy instalado e configurado para atuar como Frontend Load Balancer 

Segue as configurações principais deste ambiente:

Após a instalação do haproxy via ansble, foi adicinado ao arquivo haproxy.cfg as seguintes linha no final do arquivo:

```
frontend kubernets
        mode tcp
        bind 172.27.11.100:6443
        option tcplog
        default_backend k8s-master

backend k8s-master
        mode tcp
        balance roundrobin
        option tcp-check
        server master1 172.27.11.11:6443 check fall 3 rise 2
        server master2 172.27.11.12:6443 check fall 3 rise 2
        server master3 172.27.11.13:6443 check fall 3 rise 2

```

Foi criar o arquivo /etc/rsyslog.d/haproxy.conf
```
local2.=info     /var/log/haproxy-access.log    #For Access Log
local2.notice    /var/log/haproxy-info.log      #For Service Info - Backend, loadbalancer
```

No /etc/rsyslog.conf  descomentamos as seguintes linha
```
Provides UDP syslog reception
$ModLoad imudp
$UDPServerRun 514

```

Reiniciamos o serviço do rsyslog e haproxy:
```
sudo systemctl start haproxy.service
sudo systemctl enable haproxy.service
sudo systemctl restart rsyslog
```