# Gerando os arquivos de configuração do Kubernetes para Autenticação

Iremos gerar os arquivos de configuração dos componentes do Kubernetes, conhecido como kubeconfig, ao qual irá habilitar os clientes a comunicar com o Kubernetes API Server.

## Configuração dos Clientes

Nesta sessão você irá gerar os arquivos kubeconfig para cada serviço do Kubernetes (controller manager, kubelet, kube-proxy, e scheduler) e para o usuário administrador.

Endereço IP público Kubernetes API
Cada arquivo de configuração requer acesso a API do Kubernetes (kubeconfig). Para suportar a alta disponibilidade no acesso ao serviço Kubernetes API Server o IP do Load Balance será utilizado para acessar a API.


# Arquivos de configuração do Kubelet

Quando gerar o arquivo de configuração do Kubernetes (kubeconfig) para o serviço Kubelet, o nome do certificado (CN) tem que ser igual ao nome da instância (worker). Este processo irá garantir a autorização na comunicação do Kubelet com o API Server via Kubernetes Node Authorizer. 

Gerar o arquivo kubeconfig para cada worker:

No diretorio GerarKubeConfis na raiz do lab, executar os seguintes escripts:

```
# cd GerarKubeConfis/
# vim gerarKubeConfigWorks.sh
```


```
#!/bin/bash

KUBERNETES_PUBLIC_ADDRESS="172.27.11.100"

for instance in node1.k8s.multimaster.raiz node2.k8s.multimaster.raiz  node3.k8s.multimaster.raiz; do
  kubectl config set-cluster kubernetes-raiz \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=${instance}.kubeconfig

   kubectl config set-credentials system:node:${instance} \
    --client-certificate=../GeraCertificados/${instance}.pem \
    --client-key=../GeraCertificados/${instance}-key.pem \
    --embed-certs=true \
    --kubeconfig=${instance}.kubeconfig

   kubectl config set-context default \
    --cluster=kubernetes-raiz \
    --user=system:node:${instance} \
    --kubeconfig=${instance}.kubeconfig

   kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done

# ./gerarKubeConfigWorks.sh

```
Resultado:
```
node1.k8s.hard.com.kubeconfig
node2.k8s.hard.com.kubeconfig
node3.k8s.hard.com.kubeconfig

```

# Arquivos de configuração do Kubernetes para o serviço kube-proxy

Gerar o arquivo kubeconfig para o serviço kube-proxy:

```
# vim gerarKubeConfigKubeProxy.sh

#!/bin/bash

KUBERNETES_PUBLIC_ADDRESS="172.27.11.100"

kubectl config set-cluster kubernetes-raiz \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config set-credentials system:kube-proxy \
    --client-certificate=../GeraCertificados/kube-proxy.pem \
    --client-key=../GeraCertificados/kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config set-context default \
    --cluster=kubernetes-raiz \
    --user=system:kube-proxy \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig

# ./gerarKubeConfigKubeProxy.sh

```
## Resultados
```
kube-proxy.kubeconfig

```

## Arquivos de configuração do Kubernetes para o serviço kube-controller-manager

Gerar o arquivo kubeconfig para o serviço kube-controller-manager:

```
# vim gerarKubeConfigKubeControllerManager.sh

#!/bin/bash

kubectl config set-cluster kubernetes-raiz \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-credentials system:kube-controller-manager \
    --client-certificate=../GeraCertificados/kube-controller-manager.pem \
    --client-key=../GeraCertificados/kube-controller-manager-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-raiz \
    --user=system:kube-controller-manager \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config use-context default --kubeconfig=kube-controller-manager.kubeconfig

  # ./gerarKubeConfigKubeControllerManager.sh

```

# Resultado
```
kube-controller-manager.kubeconfig
```

## Arquivos de configuração do Kubernetes para o serviço kube-scheduler

Gerar o arquivo kubeconfig para o serviço kube-scheduler:

```
# vim gerarKubeConfigKubeSheduler.sh

#!/bin/bash

kubectl config set-cluster kubernetes-raiz \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-scheduler.kubeconfig

kubectl config set-credentials system:kube-scheduler \
    --client-certificate=../GeraCertificados/kube-scheduler.pem \
    --client-key=../GeraCertificados/kube-scheduler-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-scheduler.kubeconfig

kubectl config set-context default \
    --cluster=kubernetes-raiz \
    --user=system:kube-scheduler \
    --kubeconfig=kube-scheduler.kubeconfig


kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig


#  ./gerarKubeConfigKubeSheduler.sh                                                           
```

## Resultado:
```
kube-scheduler.kubeconfig
```

## Arquivos de configuração do Kubernetes para o usuário admin
Gerar o arquivo kubeconfig para o usuário admin:

```
# vim gerarKubeConfigKubeAdmin.sh

#!/bin/bash

kubectl config set-cluster kubernetes-raiz \
    --certificate-authority=../GeraCertificados/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=admin.kubeconfig

kubectl config set-credentials admin \
    --client-certificate=../GeraCertificados/admin.pem \
    --client-key=../GeraCertificados/admin-key.pem \
    --embed-certs=true \
    --kubeconfig=admin.kubeconfig

kubectl config set-context default \
    --cluster=kubernetes-raiz \
    --user=admin \
    --kubeconfig=admin.kubeconfig

kubectl config use-context default --kubeconfig=admin.kubeconfig


# ./gerarKubeConfigKubeAdmin.sh

```

## Resultado 
```
admin.kubeconfig
```

# Distribuir os arquivos gerados para cada node (worker) do cluster Kubernetes

Copiar o arquivo kubeconfig correspondente a cada serviço kubelet e kube-proxy para cada node (worker):

```
# vim ./distribuirArquivosDeConfiguracao.sh

#!/bin/bash


for instance in node1.k8s.multimasters.raiz node2.k8s.multimasters.raiz node3.k8s.multimasters.raiz; do
  scp -C ${instance}.kubeconfig kube-proxy.kubeconfig root@${instance}:/root/
done

for instance in master1.k8s.multimasters.raiz master2.k8s.multimasters.raiz master3.k8s.multimasters.raiz; do
  scp -C admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig root@${instance}:/root
done




# ./distribuirArquivosDeConfiguracao.sh
```
## Resultado 
```
# ./checaraquivosDeConfiguracao.sh 

Checar arquivos de configuração do kubernetes do master1
-rw------- 1 vagrant vagrant 6205 Abr 14 17:38 /home/vagrant/admin.kubeconfig
-rw------- 1 vagrant vagrant 6331 Abr 14 17:38 /home/vagrant/kube-controller-manager.kubeconfig
-rw------- 1 vagrant vagrant 6285 Abr 14 17:38 /home/vagrant/kube-scheduler.kubeconfig
############################################################
Checar arquivos de configuração do kubernetes do master2
Warning: the ECDSA host key for 'master2' differs from the key for the IP address '172.27.11.12'
Offending key for IP in /home/alexandre.silva/.ssh/known_hosts:145
Matching host key in /home/alexandre.silva/.ssh/known_hosts:149
-rw-------. 1 vagrant vagrant 6205 Apr 14 20:38 /home/vagrant/admin.kubeconfig
-rw-------. 1 vagrant vagrant 6331 Apr 14 20:38 /home/vagrant/kube-controller-manager.kubeconfig
-rw-------. 1 vagrant vagrant 6285 Apr 14 20:38 /home/vagrant/kube-scheduler.kubeconfig
############################################################
Checar arquivos de configuração do kubernetes do master3
-rw------- 1 vagrant vagrant 6205 Abr 14 17:38 /home/vagrant/admin.kubeconfig
-rw------- 1 vagrant vagrant 6285 Abr 14 17:38 /home/vagrant/kube-scheduler.kubeconfig
-rw------- 1 vagrant vagrant 6331 Abr 14 17:38 /home/vagrant/kube-controller-manager.kubeconfig
############################################################
Checar arquivos de configuração do kubernetes do node1
-rw------- 1 vagrant vagrant 6379 Abr 14 17:38 /home/vagrant/node1.k8s.hard.com.kubeconfig
-rw------- 1 vagrant vagrant 6273 Abr 14 17:38 /home/vagrant/kube-proxy.kubeconfig
Checar arquivos de configuração do kubernetes do node2
-rw------- 1 vagrant vagrant 6399 Abr 14 17:38 /home/vagrant/node2.k8s.hard.com.kubeconfig
-rw------- 1 vagrant vagrant 6273 Abr 14 17:38 /home/vagrant/kube-proxy.kubeconfig
Checar arquivos de configuração do kubernetes do node3
Warning: the ECDSA host key for 'node3' differs from the key for the IP address '172.27.11.23'
Offending key for IP in /home/alexandre.silva/.ssh/known_hosts:138
Matching host key in /home/alexandre.silva/.ssh/known_hosts:141
-rw------- 1 vagrant vagrant 6379 Abr 14 17:38 /home/vagrant/node3.k8s.hard.com.kubeconfig
-rw------- 1 vagrant vagrant 6273 Abr 14 17:38 /home/vagrant/kube-proxy.kubeconfig

```

