## VAGRANT

Para subir o ambiente utilizaremos o Vagrant para provisionar o ambiente que utilizaremos para o entendimentos do kubernetes:



```
# vim VagrantFile

# -*- mode: ruby -*-
# vi: set ft=ruby :

vms = {
  'admin-lb' => {'memory' => '1024', 'cpus' => 1, 'ip' => '100', 'provision' => 'admin-lb.sh'},
  'node1' => {'memory' => '1024', 'cpus' => 1, 'ip' => '21', 'provision' => 'prepara_maquina.sh'},
  'node2' => {'memory' => '1024', 'cpus' => 1, 'ip' => '22', 'provision' => 'prepara_maquina.sh'},
  'node3' => {'memory' => '1024', 'cpus' => 1, 'ip' => '23', 'provision' => 'prepara_maquina.sh'},
	'master1' => {'memory' => '2048', 'cpus' => 2, 'ip' => '11', 'provision' => 'prepara_maquina.sh'},
	'master2' => {'memory' => '2048', 'cpus' => 2, 'ip' => '12', 'provision' => 'prepara_maquina.sh'},
	'master3' => {'memory' => '2048', 'cpus' => 2, 'ip' => '13', 'provision' => 'prepara_maquina.sh'}
}
Vagrant.configure('2') do |config|

  config.vm.box = 'centos/7'
  config.vm.box_check_update = false

  vms.each do |name, conf|
    config.vm.define "#{name}" do |k|
      k.vm.hostname = "#{name}.k8s.com"
      k.vm.network 'private_network', ip: "172.27.11.#{conf['ip']}"
      k.vm.provider 'virtualbox' do |vb|
        vb.memory = conf['memory']
        vb.cpus = conf['cpus']
      end
      k.vm.provider 'libvirt' do |lv|
        lv.memory = conf['memory']
        lv.cpus = conf['cpus']
        lv.cputopology :sockets => 1, :cores => conf['cpus'], :threads => '1'
      end
      k.vm.provision 'shell', path: "provision/#{conf['provision']}", args: "#{conf['ip']}"
    end
  end

#  config.vm.provision 'shell', path: 'provision/master.sh'
end

```
Para subir o ambiente basta executar na maquna host na raiz do projeto: 

```
# vagrant up
```

## Ansible + Shell-Script

Utilizamos shell scrips e ansible para configurar nosso ambiente conforme os seguintes requisitos:

* Disabilitar ipv6
* Instalar e configurar NTP
* Instalar pacontes importantes (vim, telnet, nfs-tools, systemd-resolved, net-tolls e etc..)
* Desabilitar swap
* Desabilitar selinux
* Atualizar SO
* Aplicar regras de firewall

Obs.: Tadas a configurações serão realizadas ao executar o comando #vagrant up citado acima.

## Shell-script

O arquivo prepara_maquina.sh é enviado para dentro das maquinas virtuais via Vagrant: 
```
...
"k.vm.provision 'shell', path: "provision/#{conf['provision']}", args: "#{conf['ip']}""
...
```


```
#vim provision/prepara_maquina.sh

#!/bin/bash

###Instalar Ansible 2.9.6 #####
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
sudo python get-pip.py
pip install ansible==2.9.6
##############################################

## Copiar as roles do ansible para dentro da VM###
cp -Rf /vagrant/files/roles/* /etc/ansible/roles/
cp -Rf /vagrant/files/group_vars/* /etc/ansible/group_vars/
##############################################

## Executa playbook ansible com os requisitos da maquina
sudo ansible-playbook /vagrant/files/k8s_prepara_Maquinas.yml
###########################################

## Copiar chaves ###########
mkdir -p /root/.ssh
cp /vagrant/files/id_rsa* /root/.ssh
chmod 400 /root/.ssh/id_rsa*
cp /vagrant/files/id_rsa.pub /root/.ssh/authorized_keys
#############################

### Criar arquivo /etc/hosts #####
HOSTS=$(head -n7 /etc/hosts)
echo -e "$HOSTS" > /etc/hosts
echo '172.27.11.100 admin-lb.k8s.hard.com admin-lb storage.k8s.hard.com storage' >> /etc/hosts
echo '172.27.11.11 master1.k8s.hard.com master1' >> /etc/hosts
echo '172.27.11.12 master2.k8s.hard.com master2' >> /etc/hosts
echo '172.27.11.13 master3.k8s.hard.com master3' >> /etc/hosts
echo '172.27.11.21 node1.k8s.hard.com node1' >> /etc/hosts
echo '172.27.11.22 node2.k8s.hard.com node2' >> /etc/hosts
echo '172.27.11.23 node3.k8s.hard.com node3' >> /etc/hosts

if [ "$HOSTNAME" == "storage" ]; then
        exit
fi
#######################################

ssh  root@172.27.11.100 'systemctl enable haproxy.service'
ssh  root@172.27.11.100 'systemctl enable nfs-server.service'

sudo reboot
```

## Ansible
```
#vim files/k8s_prepara_Maquinas.yml

- hosts: localhost
  become: yes
  #  vars_prompt:
  #  - name: root_pwd
  #    prompt: "Enter Password: "
  #    private: yes
  #    confirm: yes
  #    salt_size: 7

  roles:
      - disable_ipv6
      - ntp
      - packages
      - ansible-firewalld   
      - disable_swap
      - server-update-reboot
```
