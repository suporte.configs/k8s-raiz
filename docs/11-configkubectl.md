# Configurar kubectl para acesso remoto

Neste lab você irá configurar o arquivo de configuração (kubeconfig) para utilizar o kubectl.

## Configuração do arquivo do Admin Kubernetes

Cada Kubeconfig precisa conectar em uma API do Kubernetes Server. Quando atuamos com alta disponibilidade o endereço do Proxy será o endereço do Kubernetes API Server.

## Gerar o arquivo Kubeconfig para autenticar o usuário admin:

```
$ KUBERNETES_PUBLIC_ADDRESS="172.27.11.100"

  $ kubectl config set-cluster kubernetes-raiz \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443

  
$ kubectl config set-credentials admin \
    --client-certificate=admin.pem \
    --client-key=admin-key.pem

  $ kubectl config set-context kubernetes-raiz \
    --cluster=kubernetes-raiz \
    --user=admin

  $ kubectl config use-context kubernetes-raiz
```

# Verificar:
Checar o estado de cada componente do Kubernets:

```
$  kubectl get componentstatuses
NAME                 STATUS    MESSAGE             ERROR
scheduler            Healthy   ok                  
controller-manager   Healthy   ok                  
etcd-1               Healthy   {"health":"true"}   
etcd-0               Healthy   {"health":"true"}   
etcd-2               Healthy   {"health":"true"}   

```

## Listar os nodes no Kuebrnetes:

```

$ kubectl get nodes

NAME                 STATUS   ROLES    AGE   VERSION
node1.k8s.hard.com   Ready    <none>   17h   v1.18.1
node2.k8s.hard.com   Ready    <none>   17h   v1.18.1
node3.k8s.hard.com   Ready    <none>   17h   v1.18.1

```
