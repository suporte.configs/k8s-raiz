# Smoke Test

Realizar testes para validar se o cluster instalado está funcionando corretamente.

## Encriptação de Dados (Data Encryption)
Validar se é possível criar um objeto criptografado secret via REST (kubectl)

## Criar um secret genérico:
```
$  kubectl create secret generic kubernetes-raiz --from-literal="minhachave=k8sRaiz"
```

Exibir - em hexdump - o objeto secret criado de nome kubernetes-raiz no serviço do etcd (master1):

```
$ sudo ETCDCTL_API=3 etcdctl get \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem\
  /registry/secrets/default/kubernetes-raiz | hexdump -C"
  
```

# Resultado:
```
00000000  2f 72 65 67 69 73 74 72  79 2f 73 65 63 72 65 74  |/registry/secret|
00000010  73 2f 64 65 66 61 75 6c  74 2f 6b 75 62 65 72 6e  |s/default/kubern|
00000020  65 74 65 73 2d 72 61 69  7a 0a 6b 38 73 3a 65 6e  |etes-raiz.k8s:en|
00000030  63 3a 61 65 73 63 62 63  3a 76 31 3a 6b 65 79 31  |c:aescbc:v1:key1|
00000040  3a 70 f2 14 d3 c9 9e 89  aa 29 bf 61 e8 23 a4 c5  |:p.......).a.#..|
00000050  e9 8d b0 69 f2 e7 e0 2c  00 54 2a 89 4c 93 f3 a4  |...i...,.T*.L...|
00000060  ca 67 b4 75 14 a5 a8 fb  16 59 f0 02 71 5a fe b3  |.g.u.....Y..qZ..|
00000070  a0 09 7b 47 18 19 42 d5  0f df aa a3 c5 61 45 a7  |..{G..B......aE.|
00000080  a5 7b b0 58 fb 02 a5 82  c8 a2 76 90 f8 85 e8 80  |.{.X......v.....|
00000090  23 90 2f c7 29 23 33 83  0c 8e fb db 0b e3 77 5e  |#./.)#3.......w^|
000000a0  87 78 14 7b 9a 66 1c 81  40 8f 36 00 02 2b 37 5d  |.x.{.f..@.6..+7]|
000000b0  d8 70 2c 37 7e 22 f7 8e  0f b9 ca 4b f1 f0 28 0b  |.p,7~".....K..(.|
000000c0  44 45 b5 9f 6d df 1e 5f  f1 40 8a a8 58 42 8b 05  |DE..m.._.@..XB..|
000000d0  e7 c9 1a fd 52 c4 16 46  7d 94 27 cf 44 08 f8 af  |....R..F}.'.D...|
000000e0  05 e1 b8 3a b9 1a d5 eb  b8 7f 45 b3 36 d2 c8 a6  |...:......E.6...|
000000f0  a2 5e 30 2b 8f 3f 64 fe  80 be 2f ba c8 c6 59 1e  |.^0+.?d.../...Y.|
00000100  08 9a 70 23 ab c9 ce 43  d7 ef 34 ae 5e 1b 12 f2  |..p#...C..4.^...|
00000110  9a 34 5c 46 c6 17 8a 1d  d9 02 09 3f 6a 50 8a 4d  |.4\F.......?jP.M|
00000120  3d 96 55 47 95 62 cf 71  27 84 88 30 bb 33 bf ca  |=.UG.b.q'..0.3..|
00000130  0a 84 1b 3d b7 23 f9 88  58 c0 79 7d c0 d3 92 d8  |...=.#..X.y}....|
00000140  31 13 00 79 7f b4 a7 55  05 3c 6f e4 a0 26 f5 75  |1..y...U.<o..&.u|
00000150  42 0a                                             |B.|
00000152
```
A chave etcd tem que o prefixo k8s:enc:aescbc:v1:key1, que indicará que o tipo de encriptação aescbc irá ser usado para encriptar os dados.

## Deployments
Nesta validação você irá criar e gerenciar Deployments.

Criar um deployment do tipo nginx:
```
$ kubectl create deployment nginx --image=nginx
```

## Listar os pods criados para o deployment nginx:

```
kubectl get pods -l run=nginx

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          31s
```

## Port Forwarding
Validar se o encaminhamento de portas está funcionando

Recuperar o nome completo do pod do nginx:

```
POD_NAME=$(kubectl get pods -l app=nginx -o jsonpath="{.items[0].metadata.name}")

```
Criar o encaminhamento da porta 8080 do seu hostlocal para a porta 80 do pod do nginx:
```
$ kubectl port-forward $POD_NAME 8080:80

Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
```
Em um novo terminal realize a requisição para o encaminhamento criado:

```
curl --head http://127.0.0.1:8080
HTTP/1.1 200 OK
Server: nginx/1.17.10
Date: Sat, 18 Apr 2020 22:08:22 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Tue, 14 Apr 2020 14:19:26 GMT
Connection: keep-alive
ETag: "5e95c66e-264"
Accept-Ranges: bytes
```
Volte ao terminal que foi criado o encaminhamento de portas e pare o encaminhamento:
```
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
Handling connection for 8080
```

## Logs
Verificar se o cluster está retornando o log dos pods.

Exibir o log do pod do :

```
$ kubectl logs $POD_NAME

127.0.0.1 - - [18/Apr/2020:22:08:22 +0000] "HEAD / HTTP/1.1" 200 0 "-" "curl/7.58.0" "-"

```
## Exec
Verificar se o cluster está executando comandos dentro do container de um pod.

Exibir a versão do nginx executando o comando "nginx -v command" dentro do container do nginx:
```
$ kubectl exec -ti $POD_NAME -- nginx -v

nginx version: nginx/1.17.10

```

## Services
Verificar se a capacidade do ambiente de expor uma aplicaçÃo usando o Service do Kubernetes.

Criar um service para o nginx do tipo NodePort:
```
$ kubectl expose deployment nginx --port 80 --type NodePort
```

Recupere a porta do tipo Node Port que foi atribuída para o serviço do nginx:
```
$ NODE_PORT=$(kubectl get svc nginx \
  --output=jsonpath='{range .spec.ports[0]}{.nodePort}')

Recupere o endereço IP externo de uma instância do work:

$ EXTERNAL_IP="172.27.11.21"
```

Faça uma requisição do tipo HTTP no porta do serviço do nginx:

```
$ curl -I http://${EXTERNAL_IP}:${NODE_PORT}

HTTP/1.1 200 OK
Server: nginx/1.15.4
Date: Sun, 30 Sep 2018 19:25:40 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Tue, 25 Sep 2018 15:04:03 GMT
Connection: keep-alive
ETag: "5baa4e63-264"
Accept-Ranges: bytes
```

