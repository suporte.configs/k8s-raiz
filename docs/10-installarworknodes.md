# Instalar o Kubernetes Worker Nodes

Neste lab você irá realizar a instalação de 3 (três) workers nodes. Os seguintes componentes irão ser instalados: runc, gVisor, container networking plugins, containerd, kubelet, e kube-proxy

## Pré-requisitos
O comandos abaixo serão executados em cada node definido com worker.

## Provisioning a Kubernetes Worker Node
Instalar as dependência de sistema operacional:
```
$  apt-get update
   apt-get -y install socat conntrack ipset

systemctl enable systemd-resolved.service
systemctl start systemd-resolved.service
```
O binário socat habilita o suporte ao kubectl port-forward command.

## Download e Instalação dos binários para os nodes Worker
```
wget --timestamping \
  https://storage.googleapis.com/gvisor/releases/nightly/latest/runsc \
  https://storage.googleapis.com/gvisor/releases/nightly/latest/runsc.sha512 \
  https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.18.0/crictl-v1.18.0-linux-amd64.tar.gz \
  https://github.com/opencontainers/runc/releases/download/v1.0.0-rc10/runc.amd64 \
  https://github.com/containernetworking/plugins/releases/download/v0.8.5/cni-plugins-linux-amd64-v0.8.5.tgz \
  https://github.com/containerd/containerd/releases/download/v1.3.3/containerd-1.3.3.linux-amd64.tar.gz \
  https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kubectl \
  https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kube-proxy \
  https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kubelet
```

## Criar e instalar os diretórios:
```
sudo mkdir -p \
  /etc/cni/net.d \
  /opt/cni/bin \
  /var/lib/kubelet \
  /var/lib/kube-proxy \
  /var/lib/kubernetes \
  /var/run/kubernetes
```
## Instalar os binários nos nodes workers:
```
  sha512sum -c runsc.sha512
  chmod +x runsc
  sudo mv runsc /usr/local/bin 	
  sudo mv runc.amd64 runc
  sudo chmod +x kubectl kube-proxy kubelet runc runsc
  sudo mv kubectl kube-proxy kubelet runc runsc /usr/local/bin/
  sudo tar -xvf crictl-v1.18.0-linux-amd64.tar.gz -C /usr/local/bin/
  sudo tar -xvf cni-plugins-linux-amd64-v0.8.5.tgz -C /opt/cni/bin/
  sudo tar -xvf containerd-1.3.3.linux-amd64.tar.gz -C /tmp
  sudo mv /tmp/bin/* /bin/
  rm -rf /tmp/bin
```

## Configurar a rede CNI 
```
node1 - POD_CIDR=192.167.21.0/24
node2 - POD_CIDR=192.167.22.0/24
node3 - POD_CIDR=192.167.23.0/24
```
Obs.: cada node tem que ter um range diferente

## Criar os arquivos de configurações da rede Bridge:
```
$ cat <<EOF | sudo tee /etc/cni/net.d/10-bridge.conf
{
    "cniVersion": "0.3.1",
    "name": "bridge",
    "type": "bridge",
    "bridge": "cnio0",
    "isGateway": true,
    "ipMasq": true,
    "ipam": {
        "type": "host-local",
        "ranges": [
          [{"subnet": "${POD_CIDR}"}]
        ],
        "routes": [{"dst": "0.0.0.0/0"}]
    }
}
EOF
```

## Criar os arquivos de configuração de rede do loopback:
```
$ cat <<EOF | sudo tee /etc/cni/net.d/99-loopback.conf
{
    "cniVersion": "0.3.1",
    "type": "loopback"
}
EOF
```
## Configurar o Containerd

## Criar os arquivos de configuração do Containerd:

```
sudo mkdir -p /etc/containerd/
$ cat << EOF | sudo tee /etc/containerd/config.toml
[plugins]
  [plugins.cri.containerd]
    snapshotter = "overlayfs"
    [plugins.cri.containerd.default_runtime]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "/usr/local/bin/runc"
      runtime_root = ""
    [plugins.cri.containerd.untrusted_workload_runtime]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "/usr/local/bin/runsc"
      runtime_root = "/run/containerd/runsc"
    [plugins.cri.containerd.gvisor]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "/usr/local/bin/runsc"
      runtime_root = "/run/containerd/runsc"
EOF

```
Cargas de trabalho não confiáveis serão executadas usando o gVisor (runsc).

## Criar o arquivo systemd unit para containerd.service:

```
$ cat <<EOF | sudo tee /etc/systemd/system/containerd.service
[Unit]
Description=containerd container runtime
Documentation=https://containerd.io
After=network.target

[Service]
ExecStartPre=/sbin/modprobe overlay
ExecStart=/bin/containerd
Restart=always
RestartSec=5
Delegate=yes
KillMode=process
OOMScoreAdjust=-999
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity

[Install]
WantedBy=multi-user.target
EOF
```
## Configurar o Kubelet
```
sudo mv ${HOSTNAME}-key.pem ${HOSTNAME}.pem /var/lib/kubelet/
sudo mv ${HOSTNAME}.kubeconfig /var/lib/kubelet/kubeconfig
sudo mv ca.pem /var/lib/kubernetes/
```
## Criar os arquivos de configuração kubelet-config.yaml:
```
$ cat <<EOF | sudo tee /var/lib/kubelet/kubelet-config.yaml
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    enabled: true
  x509:
    clientCAFile: "/var/lib/kubernetes/ca.pem"
authorization:
  mode: Webhook
clusterDomain: "cluster.local"
clusterDNS:
  - "10.32.0.10"
podCIDR: "${POD_CIDR}"
resolvConf: "/run/systemd/resolve/resolv.conf"
runtimeRequestTimeout: "15m"
tlsCertFile: "/var/lib/kubelet/${HOSTNAME}.pem"
tlsPrivateKeyFile: "/var/lib/kubelet/${HOSTNAME}-key.pem"
EOF
```

As configurações do resolvConf são usadas para evitar loops quando usado com o serviço de descoberta do  CoreDNS.

## Criar o arquivo systemd unit kubelet.service:
```
$ cat <<EOF | sudo tee /etc/systemd/system/kubelet.service
[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/kubernetes/kubernetes
After=containerd.service
Requires=containerd.service

[Service]
ExecStart=/usr/local/bin/kubelet \\
  --config=/var/lib/kubelet/kubelet-config.yaml \\
  --container-runtime=remote \\
  --container-runtime-endpoint=unix:///var/run/containerd/containerd.sock \\
  --image-pull-progress-deadline=2m \\
  --kubeconfig=/var/lib/kubelet/kubeconfig \\
  --network-plugin=cni \\
  --register-node=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

## Configurar o Kubernetes Proxy
```
sudo mv kube-proxy.kubeconfig /var/lib/kube-proxy/kubeconfig
```

## Criar o arquivo de configuração kube-proxy-config.yaml:
```
$ cat <<EOF | sudo tee /var/lib/kube-proxy/kube-proxy-config.yaml
kind: KubeProxyConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
clientConnection:
  kubeconfig: "/var/lib/kube-proxy/kubeconfig"
mode: "iptables"
clusterCIDR: "10.200.0.0/16"
EOF
```

## Criar o arquivo systemd unit kube-proxy.service:
```
$ cat <<EOF | sudo tee /etc/systemd/system/kube-proxy.service
[Unit]
Description=Kubernetes Kube Proxy
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-proxy \\
  --config=/var/lib/kube-proxy/kube-proxy-config.yaml
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```
## Inicializar os serviços do Worker
```
 sudo systemctl daemon-reload
 sudo systemctl enable containerd kubelet kube-proxy
 sudo systemctl start containerd kubelet kube-proxy
```
Lembre-se de executar estes comandos em cada servidor definido como worker, neste curso: worker-0, worker-1


## Verificação

Listar os nodes Kubernetes:

[vagrant@master1 ~]$ kubectl get nodes --kubeconfig admin.kubeconfig
NAME                 STATUS   ROLES    AGE   VERSION
node1.k8s.hard.com   Ready    <none>   50s   v1.18.1
node2.k8s.hard.com   Ready    <none>   50s   v1.18.1
node3.k8s.hard.com   Ready    <none>   50s   v1.18.1
