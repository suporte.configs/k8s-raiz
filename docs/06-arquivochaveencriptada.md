
# Gerando os arquivos de configuração do Kubernetes para Autenticação

Gerando encriptação de dados de configuração e chave

Kubernetes armazena uma variedade de dados incluindo o estado do cluster, configurações de aplicações, e secrets. Por este motivo o Kubernetes realiza a encriptação do cluster via REST.

Neste curso você irá gerar uma chave e configuração criptografada adequada para realizar a encriptação dos "Secrets" do Kubernetes.

## A Chave criptográfica

Gerar uma chave criptográfica:

ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)

## O arquivo de configuração 

O arquivo de configuração de criptografia:

```
cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
```

Ev

Copiar o arquivo de configuração criptografado encryption-config.yaml para cada controller:


```
# ./distribuirArquivoConfiguracaoCriptografia.sh 
```