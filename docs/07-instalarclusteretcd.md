# Instalando Cluster ETCD

https://etcd.io/

O ETCD é o armazenamento de alta disponibilidade de informações chamadas de consistentes. Ele é utilizado como armazenamento de apoio para todos os dados de clusters do Kubernetes.

É um sistema distribuído de armazenamento do tipo key valor ou key=valor, utilizado pelo Kubernetes para armazenar todas informações sobre os Pods, containers, serviços, redes, nós do cluster, localização dos containeres, Cpus, Memória, versões de aplicativos e metados no geral.

Os componentes do Kubernetes são "stateless" e armazenam seus estados no etcd. Neste laboratorio você irá realizar a instalação (bootstrapping) do etcd em 3 (três) nodes e configurar a alta disponibilidade e acesso seguro remoto.

Se o seu cluster de Kubernetes usa o ETCD como seu repositório de apoio, é importante vocÊ se certificar de que tem um plano de backup para esses dados.

## Pré-requisitos

Os comandos deverão ser executados nos servidores controller (master): master1, master2 e master3:

Obs: Você pode executar os comandos paralelamente usando tmux ou terminator:

Instalando o cluster etcd
Realizar o Download e Instalar os binários do etcd:

```
# ETCD_VER=v3.4.7

# GOOGLE_URL=https://storage.googleapis.com/etcd

# GITHUB_URL=https://github.com/etcd-io/etcd/releases/download

# DOWNLOAD_URL=${GOOGLE_URL}

# rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
# rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test

# curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

# tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd-download-test --strip-components=1

# rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

# /tmp/etcd-download-test/etcd --version
# /tmp/etcd-download-test/etcdctl version

# mv /tmp/etcd-download-test/etcd* /usr/local/bin/
```
## Configurar os servidores membros do ETCD
```
# su - vagrant

# sudo mkdir -p /etc/etcd /var/lib/etcd

# sudo cp ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/
```
O endereço IP interno da instância será utilizado para a comunicação e requisição entre os membros do cluster etcd.

```
# INTERNAL_IP=$(ip addr show eth1 | grep "inet " | awk '{print $2}' | cut -d / -f 1)

```
Cada membro do cluster etcd deve ter um único nome. Configure o etcd para que o nome seja igual o nome do host (hostname):

```
ETCD_NAME=$(hostname -s)
master1="172.27.11.11"
master2="172.27.11.12"
master3="172.27.11.13"
```

Criar o arquivo etcd.service para o systemd unit:
```
cat <<EOF | sudo tee /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
ExecStart=/usr/local/bin/etcd \\
  --name ${ETCD_NAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-client-urls https://${INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
  --advertise-client-urls https://${INTERNAL_IP}:2379 \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster master1=https://${master1}:2380,master2=https://${master2}:2380,master3=https://${master3}:2380 \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

Iniciar o serviço do etcd

```
sudo systemctl daemon-reload
sudo systemctl enable etcd
sudo systemctl start etcd
```
Não esquecer de rodar os comandos acima em todos os nós que irá ser um membro etcd: master1, master2 e master3.

## Verificação
Listar os membros do cluster etcd:
```
sudo etcdctl --cacert /etc/etcd/ca.pem --cert /etc/etcd/kubernetes.pem --key /etc/etcd/kubernetes-key.pem member list
```

## Resultados:
```
1b0fed455eac3f1d, started, master1, https://172.27.11.11:2380, https://172.27.11.11:2379, false
83d518997940ddb2, started, master2, https://172.27.11.12:2380, https://172.27.11.12:2379, false
d5a78816a8ce535d, started, master3, https://172.27.11.13:2380, https://172.27.11.13:2379, false

```

Listar se os membros do cluster etcd estão validados:

```
sudo etcdctl --cacert /etc/etcd/ca.pem --cert /etc/etcd/kubernetes.pem --key /etc/etcd/kubernetes-key.pem endpoint --cluster health
```

```
https://172.27.11.13:2379 is healthy: successfully committed proposal: took = 11.09782ms
https://172.27.11.11:2379 is healthy: successfully committed proposal: took = 11.745722ms
https://172.27.11.12:2379 is healthy: successfully committed proposal: took = 11.881758ms
```