# CFSSL 

Neste laboratorio vamos provisionar uma infraestrutura PKI usando o kit de ferramentas PKI do CloudFlare,cfss, em seguida, utilizá-lo para inicializar uma CA e gerar certificados TLS para os seguintes componentes do kubernetes: etcd, kube-apiserver, kube-controller-manager, kube-controller, manager, kubelet e kube-proxy.

O CFSSL é um kit de ferramentas de código aberto, escrito em GO,  para tudo que é TLS/SSL. É usado internamente pelo CloudFlare para agrupar cadeias de certificados TLS/SLL e para infrarstrutura interna como Autoridade de Certificado.

* https://blog.cloudflare.com/introducing-cfssl/
* https://github.com/cloudflare/cfssl

## Instalar CFSSL

No servidor host, vamos executar as seguintes instalações:
```
[vagrant@admin-lb ~]$ wget  --timestamping   https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/linux/cfssl   https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/linux/cfssljson
```
```
[vagrant@admin-lb ~]$ chmod +x cfssl cfssljson
```
```
[vagrant@admin-lb ~]$ sudo mv cfssl cfssljson /usr/local/bin/
```
```
[vagrant@admin-lb ~]$ cfssl version
Version: 1.3.4
Revision: dev
Runtime: go1.13
```
```
[vagrant@admin-lb ~]$ cfssljson --version
Version: 1.3.4
Revision: dev
Runtime: go1.13
```
## Instalar Kubectl

A ferramenta de linha de comando kubectl é um utilitário usado para interagir com a API do Kubernetes (Kube-apiserver). Para continuar nosso laboratorio vamos fazer o dowload e instalar apartir dos binários oficiais da versão:
```
[vagrant@admin-lb ~]$ wget https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kubectl
```

```
[vagrant@admin-lb ~]$ chmod +x kubectl
```
```
[vagrant@admin-lb ~]$ sudo mv kubectl /usr/local/bin/
```
```
[vagrant@admin-lb ~]$ kubectl version --client
Client Version: version.Info{Major:"1", Minor:"18", GitVersion:"v1.18.1", GitCommit:"7879fc12a63337efff607952a323df90cdc7a335", GitTreeState:"clean", BuildDate:"2020-04-08T17:38:50Z", GoVersion:"go1.13.9", Compiler:"gc", Platform:"linux/amd64"}
```