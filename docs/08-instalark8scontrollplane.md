# Instalando o Kubernetes Control Plane

Neste curso você irá instalar o Kubernetes control plane em três instâncias e configurar para atuar em alta disponibilidade. Você irá criar um load balance (HAProxy) para publicar o serviço API Server para os clientes remotos.

Os seguintes componentes serão instalados em cada nó: Kubernetes API Server, Scheduler, e Controller Manager.

## Pré-requisitos
O comandos abaixo devem ser executados em cada controller (master): master1,master2 e master3.

## Provisionar o Kubernetes Control Plane
Criar o diretório para as configurações do Kubernetes:

```
$ sudo mkdir -p /etc/kubernetes/config
```

## Instalação dos binários do Kubernetes Controller Binaries
Download das versões oficiais do Kubernetes:
```
wget "https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kube-apiserver" "https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kube-controller-manager" "https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kube-scheduler" "https://storage.googleapis.com/kubernetes-release/release/v1.18.1/bin/linux/amd64/kubectl" "https://storage.googleapis.com/kubernetes-release/release/stable.txt"
```

## Instalar os binários do Kubernetes:
```
$ chmod +x kube-apiserver kube-controller-manager kube-scheduler kubectl
$ sudo mv kube-apiserver kube-controller-manager kube-scheduler kubectl /usr/local/bin/
```

## Configurar o Kubernetes API Server
```
sudo mkdir -p /var/lib/kubernetes/
```

```
sudo mv ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
service-account-key.pem service-account.pem \
encryption-config.yaml /var/lib/kubernetes/
```

```
INTERNAL_IP=$(ip addr show eth1 | grep "inet " | awk '{print $2}' | cut -d / -f 1)
 
```
## Criar o arquivo systemd unit kube-apiserver.service:

Referências: 

https://kubernetes.io/docs/reference/command-line-tools-reference/kube-apiserver/

https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/

```
cat <<EOF | sudo tee /etc/systemd/system/kube-apiserver.service
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-apiserver \\
  --advertise-address=172.27.11.11 \\
  --allow-privileged=true \\
  --apiserver-count=3 \\
  --audit-log-maxage=30 \\
  --audit-log-maxbackup=3 \\
  --audit-log-maxsize=100 \\
  --audit-log-path=/var/log/audit.log \\
  --authorization-mode=Node,RBAC \\
  --bind-address=0.0.0.0 \\
  --client-ca-file=/var/lib/kubernetes/ca.pem \\
--enable-admission-plugins=NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota,PersistentVolumeClaimResize,PodPreset\
  --etcd-cafile=/var/lib/kubernetes/ca.pem \\
  --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
  --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
  --etcd-servers=https://172.27.11.11:2379,https://172.27.11.12:2379,https://172.27.11.13:2379 \\
  --event-ttl=1h \\
  --encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
  --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
  --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
  --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
  --kubelet-https=true \\
  --runtime-config=api/all=true \\
  --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --service-node-port-range=30000-32767 \\
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

## Configurar o Kubernetes Controller Manager
Mover o kube-controller-manager kubeconfig para o local:

```
$ sudo mv kube-controller-manager.kubeconfig /var/lib/kubernetes/
```

## Criar o arquivo systemd unit kube-controller-manager.service:

https://kubernetes.io/docs/reference/command-line-tools-reference/kube-controller-manager/

```
cat <<EOF | sudo tee /etc/systemd/system/kube-controller-manager.service
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-controller-manager \\
  --address=0.0.0.0 \\
  --cluster-cidr=10.200.0.0/16 \\
  --cluster-name=kubernetes-raiz \\
  --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
  --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
  --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
  --leader-elect=true \\
  --root-ca-file=/var/lib/kubernetes/ca.pem \\
  --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --use-service-account-credentials=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

## Configurar o Kubernetes Scheduler

Mover o kube-scheduler kubeconfig para o local:

```
$ sudo mv kube-scheduler.kubeconfig /var/lib/kubernetes/
```
## Criar o arquivo de configuração kube-scheduler.yaml:
```
cat <<EOF | sudo tee /etc/kubernetes/config/kube-scheduler.yaml
apiVersion: kubescheduler.config.k8s.io/v1alpha1
kind: KubeSchedulerConfiguration
clientConnection:
  kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
leaderElection:
  leaderElect: true
EOF
```

## Criar o arquivo systemd unit kube-scheduler.service systemd:
```
cat <<EOF | sudo tee /etc/systemd/system/kube-scheduler.service
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-scheduler \\
  --config=/etc/kubernetes/config/kube-scheduler.yaml \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

## Iniciar os serviços do Controller
```
  $ sudo systemctl daemon-reload
  $ sudo systemctl enable kube-apiserver kube-controller-manager kube-scheduler
  $ sudo systemctl start kube-apiserver kube-controller-manager kube-scheduler
```

Aguardar cerca de 10 segundos para o serviço do kubernetes API Server tenha inicializado por completo.

## Verificação:
```
[vagrant@master1 ~]$  kubectl get componentstatuses --kubeconfig admin.kubeconfig
NAME                 STATUS    MESSAGE             ERROR
controller-manager   Healthy   ok                  
etcd-1               Healthy   {"health":"true"}   
scheduler            Healthy   ok                  
etcd-2               Healthy   {"health":"true"}   
etcd-0               Healthy   {"health":"true"}   

```

Lembre-se de executar estes comandos em todos os nodes do controller (master): master1, master2 e master3:

## RBAC for Kubelet Authorization

Nesta sessão você irá configurar permissões RBAC para liberar o acesso ao Kubernetes API Server vindo de cada node (worker). Acesso ao API Server é necessário para consultar métricas, logs e executar comandos nos pods.

Neste lab, configuramos o Kubelet --authorization-mode flag para Webhook. O modo Webhook usa o SubjectAccessReview API para determinar a autorização.

## SSH para master1
Criar o ClusterRole system:kube-apiserver-to-kubelet com as permissões para acessar a API do Kubernetes e executar as tarefas mais comuns associada a gerência de pods:

```
$ cat <<EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  labels:
    kubernetes.io/bootstrapping: rbac-defaults
  name: system:kube-apiserver-to-kubelet
rules:
  - apiGroups:
      - ""
    resources:
      - nodes/proxy
      - nodes/stats
      - nodes/log
      - nodes/spec
      - nodes/metrics
    verbs:
      - "*"
EOF
```

A autenticação do Kubernetes API Server para o Kubelet será como uma autenticação de usuário baseado em chaves definido pela flag  --kubelet-client-certificate.

Realizar o Bind system:kube-apiserver-to-kubelet do ClusterRole para o usuário do Kubernetes:
```
cat <<EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: system:kube-apiserver
  namespace: ""
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:kube-apiserver-to-kubelet
subjects:
  - apiGroup: rbac.authorization.k8s.io
    kind: User
    name: kubernetes
EOF
```
