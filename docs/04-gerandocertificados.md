# Gerando Certificados

Neste laboratório você irá provisionar uma infraestrutura PKI usando a ferramenta de PKI da CloudFalre (CFSSL), usaremos esta ferramenta para criar uma autoridade certificadora e gerar os certificados TLS para os seguintes componentes: etcd, kube-apiserver, kube-controller-manager, kube-scheduler, kubelet and kube-proxy.

## Autoridade Certificadora (executar no servidor HOST)
Nesta sessão você irá provisionar uma autoridade certificadora que será usada para gerar os certificados adicionais necessários para o cluster Kubernetes.

```
cd  GeraCertificados/
$ vim ca-config.json

{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
```
```
$ vim ca-csr.json    

{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "DF"
    }
  ]
}

```
## Resultados esperados:
```
[vagrant@admin-lb jsons]$ ls -ltr 
total 20
-rw-rw-r--. 1 vagrant vagrant  232 Apr 13 05:38 ca-config.json
-rw-rw-r--. 1 vagrant vagrant  207 Apr 13 05:40 ca-csr.json
```
```
[vagrant@admin-lb jsons]$ cfssl gencert -initca ca-csr.json | cfssljson -bare ca
2020/04/13 06:07:45 [INFO] generating a new CA key and certificate from CSR
2020/04/13 06:07:45 [INFO] generate received request
2020/04/13 06:07:45 [INFO] received CSR
2020/04/13 06:07:45 [INFO] generating key: rsa-2048
2020/04/13 06:07:45 [INFO] encoded CSR
2020/04/13 06:07:45 [INFO] signed certificate with serial number 688187703435844596447358667584346607619605042457
```
## Resultados esperados:
```
[vagrant@admin-lb jsons]$ ls -ltr

-rw-rw-r--. 1 vagrant vagrant 1306 Apr 13 06:07 ca.pem
-rw-r--r--. 1 vagrant vagrant 1001 Apr 13 06:07 ca.csr
-rw-------. 1 vagrant vagrant 1675 Apr 13 06:07 ca-key.pem

```

## Certificados Server e Cliente
Nesta sessão você irá gerar os certificados para o server e cliente de cada componente do Kubernetes e um certificado cliente para o usuário administrador do Kubernetes. 

## Certificado do usuário Administrador
Gerar o certificado cliente e chave privada do usuário admin

```
# vim admin-csr.json 

{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "system:masters",
      "OU": "Kubernetes RAIZ",
      "ST": "DF"
    }
  ]
}

```
```
# cfssl gencert   -ca=ca.pem   -ca-key=ca-key.pem   -config=ca-config.json   -profile=kubernetes  admin-csr.json | cfssljson -bare admin

2020/04/13 06:08:51 [INFO] generate received request
2020/04/13 06:08:51 [INFO] received CSR
2020/04/13 06:08:51 [INFO] generating key: rsa-2048
2020/04/13 06:08:52 [INFO] encoded CSR
2020/04/13 06:08:52 [INFO] signed certificate with serial number 199976460507650225524807614247571968213354652081
2020/04/13 06:08:52 [WARNING] This certificate lacks a "hosts" field. This makes it unsuitable for
websites. For more information see the Baseline Requirements for the Issuance and Management
of Publicly-Trusted Certificates, v.1.1.6, from the CA/Browser Forum (https://cabforum.org);
specifically, section 10.2.3 ("Information Requirements").

```
## Resultados esperados:
```
-rw-rw-r--. 1 vagrant vagrant 1407 Apr 13 06:08 admin.pem
-rw-r--r--. 1 vagrant vagrant 1017 Apr 13 06:08 admin.csr
-rw-------. 1 vagrant vagrant 1679 Apr 13 06:08 admin-key.pem
```

## Certificados cliente do Kubelet

O Kubernetes usa um propósito especial na autorização, chamado Node Authorizer, este especificamente autoriza as requisições para a API (kube-api-server) vindas do Kubelet (worker). Para ser autorizado pelo Node Authorizer o Kubelet tem que usar a credencial identificada como grupo system:nodes, com nome de usuário sendo system:node:<NodeName>. Nesta sessão você irá criar o certificado para cada Worker ao qual irá atender os requisitos do Node Authorizer.

## Gerar o certificado e chave privada para cada worker

```
[vagrant@admin-lb jsons]$ chmod +x certificado_chave_privada_works.sh 

```

```
[vagrant@admin-lb jsons]$ vim certificado_chave_privada_works.sh

#!/bin/bash

declare -A workers

workers[0,1]="node1.k8s.multimasters.raiz"
workers[0,2]="172.27.11.21"
workers[1,1]="node2.k8s.multimasters.raiz"
workers[1,2]="172.27.11.hard.22"
workers[2,1]="node3.k8s.multimasters.raiz"
workers[2,2]="172.27.11.23"

for ((workerName=0; workerName <= 2; workerName++)); do
cat > ${workers[${workerName},1]}-csr.json <<EOF
{
  "CN": "system:node:${workers[${workerName},1]}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "system:nodes",
      "OU": "Kubernetes Raiz",
      "ST": "DF"
    }
  ]
}
EOF

INTERNAL_IP="${workers[${workerName},2]}"

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${workers[${workerName},1]},${INTERNAL_IP} \
  -profile=kubernetes \
  ${workers[${workerName},1]}-csr.json | cfssljson -bare ${workers[${workerName},1]}
done

```

```
# ./certificado_chave_privada_works.sh 
2020/04/13 06:20:47 [INFO] generate received request
2020/04/13 06:20:47 [INFO] received CSR
2020/04/13 06:20:47 [INFO] generating key: rsa-2048
2020/04/13 06:20:47 [INFO] encoded CSR
2020/04/13 06:20:47 [INFO] signed certificate with serial number 211025292116839921321445988042149081574921876096
2020/04/13 06:20:47 [INFO] generate received request
2020/04/13 06:20:47 [INFO] received CSR
2020/04/13 06:20:47 [INFO] generating key: rsa-2048
2020/04/13 06:20:47 [INFO] encoded CSR
2020/04/13 06:20:47 [INFO] signed certificate with serial number 441663878917415482185673708928395454948603371692
2020/04/13 06:20:47 [INFO] generate received request
2020/04/13 06:20:47 [INFO] received CSR
2020/04/13 06:20:47 [INFO] generating key: rsa-2048
2020/04/13 06:20:47 [INFO] encoded CSR
2020/04/13 06:20:47 [INFO] signed certificate with serial number 479274830990591891853262258256832375123587696853

```

## Resultados esperados:
```
-rw-rw-r--. 1 vagrant vagrant  237 Apr 13 06:20 node1.k8s.com-csr.json
-rw-rw-r--. 1 vagrant vagrant 1476 Apr 13 06:20 node1.k8s.com.pem
-rw-r--r--. 1 vagrant vagrant 1106 Apr 13 06:20 node1.k8s.com.csr
-rw-------. 1 vagrant vagrant 1679 Apr 13 06:20 node1.k8s.com-key.pem
-rw-rw-r--. 1 vagrant vagrant  237 Apr 13 06:20 node2.k8s.com-csr.json
-rw-rw-r--. 1 vagrant vagrant 1476 Apr 13 06:20 node2.k8s.com.pem
-rw-r--r--. 1 vagrant vagrant 1106 Apr 13 06:20 node2.k8s.com.csr
-rw-------. 1 vagrant vagrant 1675 Apr 13 06:20 node2.k8s.com-key.pem
-rw-rw-r--. 1 vagrant vagrant  237 Apr 13 06:20 node3.k8s.com-csr.json
-rw-rw-r--. 1 vagrant vagrant 1476 Apr 13 06:20 node3.k8s.com.pem
-rw-r--r--. 1 vagrant vagrant 1106 Apr 13 06:20 node3.k8s.com.csr
-rw-------. 1 vagrant vagrant 1679 Apr 13 06:20 node3.k8s.com-key.pem
[vagrant@admin-lb jsons]$
```

## Certificado Ciente do Controller Manager

Gerar o certificado e chave privada do kube-controller-manager:

```
# vim kube-controller-manager-csr.json
```

```
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes RAIZ",
      "ST": "DF"
    }
  ]
}
```

```
# cfssl gencert -ca=ca.pem  -ca-key=ca-key.pem  -config=ca-config.json  -profile=kubernetes  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager


2020/04/13 06:25:09 [INFO] generate received request
2020/04/13 06:25:09 [INFO] received CSR
2020/04/13 06:25:09 [INFO] generating key: rsa-2048
2020/04/13 06:25:10 [INFO] encoded CSR
2020/04/13 06:25:10 [INFO] signed certificate with serial number 474580547845025950528258125441640522175095290058
2020/04/13 06:25:10 [WARNING] This certificate lacks a "hosts" field. This makes it unsuitable for
websites. For more information see the Baseline Requirements for the Issuance and Management
of Publicly-Trusted Certificates, v.1.1.6, from the CA/Browser Forum (https://cabforum.org);
specifically, section 10.2.3 ("Information Requirements").

```
## Resultados esperados:

```
-rw-rw-r--. 1 vagrant vagrant  260 Apr 13 06:23 kube-controller-manager-csr.json
-rw-rw-r--. 1 vagrant vagrant 1464 Apr 13 06:25 kube-controller-manager.pem
-rw-r--r--. 1 vagrant vagrant 1074 Apr 13 06:25 kube-controller-manager.csr
-rw-------. 1 vagrant vagrant 1679 Apr 13 06:25 kube-controller-manager-key.pem
```
## Certificado Cliente do Kube Proxy
Gerar o certificado e chave privada do kube-proxy
```
[vagrant@admin-lb jsons]$ vim  kube-proxy-csr.json


{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "system:node-proxier",
      "OU": "Kubernetes RAIZ",
      "ST": "DF"
    }
  ]
}
```
```
# cfssl gencert -ca=ca.pem -ca-key=ca-key.pem  -config=ca-config.json -profile=kubernetes kube-proxy-csr.json | cfssljson -bare kube-proxy


2020/04/13 06:29:47 [INFO] generate received request
2020/04/13 06:29:47 [INFO] received CSR
2020/04/13 06:29:47 [INFO] generating key: rsa-2048
2020/04/13 06:29:47 [INFO] encoded CSR
2020/04/13 06:29:47 [INFO] signed certificate with serial number 56651705208212586427199283894302927891748295030
2020/04/13 06:29:47 [WARNING] This certificate lacks a "hosts" field. This makes it unsuitable for
websites. For more information see the Baseline Requirements for the Issuance and Management
of Publicly-Trusted Certificates, v.1.1.6, from the CA/Browser Forum (https://cabforum.org);
specifically, section 10.2.3 ("Information Requirements").

```

## Resultados esperados:
```
-rw-rw-r--. 1 vagrant vagrant  236 Apr 13 06:27 kube-proxy-csr.json
-rw-rw-r--. 1 vagrant vagrant 1432 Apr 13 06:29 kube-proxy.pem
-rw-r--r--. 1 vagrant vagrant 1041 Apr 13 06:29 kube-proxy.csr
-rw-------. 1 vagrant vagrant 1675 Apr 13 06:29 kube-proxy-key.pem
```

## Certificado Cliente do Kubernetes Scheduler
Gerar o certificado e chave privada do kube-schedule:

```
# vim kube-scheduler-csr.json 


{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "system:kube-scheduler",
      "OU": "Kubernetes RAIZ",
      "ST": "DF"
    }
  ]
}
```
```
$ cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes  kube-scheduler-csr.json | cfssljson -bare kube-scheduler


2020/04/13 06:34:35 [INFO] generate received request
2020/04/13 06:34:35 [INFO] received CSR
2020/04/13 06:34:35 [INFO] generating key: rsa-2048
2020/04/13 06:34:35 [INFO] encoded CSR
2020/04/13 06:34:35 [INFO] signed certificate with serial number 32722703217524848961261203572071064785519942333
2020/04/13 06:34:35 [WARNING] This certificate lacks a "hosts" field. This makes it unsuitable for
websites. For more information see the Baseline Requirements for the Issuance and Management
of Publicly-Trusted Certificates, v.1.1.6, from the CA/Browser Forum (https://cabforum.org);
specifically, section 10.2.3 ("Information Requirements").

```

```
-rw-rw-r--. 1 vagrant vagrant 1440 Apr 13 06:34 kube-scheduler.pem
-rw-r--r--. 1 vagrant vagrant 1050 Apr 13 06:34 kube-scheduler.csr
-rw-------. 1 vagrant vagrant 1675 Apr 13 06:34 kube-scheduler-key.pem
[vagrant@admin-lb jsons]$
```
## Certificado do Kubernetes API Server
Quando trabalhamos com acesso a API do Kubernetes é necessário que todos os "meios" de acesso a API precisam ser adicionada no certificado como "assuntos alternativos (subject alternative)".

Gerar o certificado e chave privada do Kubernetes API Server:
```
KUBERNETES_PUBLIC_ADDRESS="172.27.11.100"
```

```
# vim kubernetes-csr.json 

{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "Kubernetes",
      "OU": "Kubernetes RAIZ",
      "ST": "DF"
    }
  ]
}
```

```
# cfssl gencert   -ca=ca.pem   -ca-key=ca-key.pem   -config=ca-config.json   -hostname=10.32.0.1,master1,172.27.11.11,master2,172.27.11.12,master3,172.27.11.13,${KUBERNETES_PUBLIC_ADDRESS},127.0.0.1,kubernetes.default   -profile=kubernetes   kubernetes-csr.json | cfssljson -bare kubernetes


2020/04/13 06:40:14 [INFO] generate received request
2020/04/13 06:40:14 [INFO] received CSR
2020/04/13 06:40:14 [INFO] generating key: rsa-2048
2020/04/13 06:40:14 [INFO] encoded CSR
2020/04/13 06:40:14 [INFO] signed certificate with serial number 700758747675594841991378084458932338264964596338

```
## Resultados esperados:

```
-rw-rw-r--. 1 vagrant vagrant 1529 Apr 13 06:40 kubernetes.pem
-rw-r--r--. 1 vagrant vagrant 1159 Apr 13 06:40 kubernetes.csr
-rw-------. 1 vagrant vagrant 1675 Apr 13 06:40 kubernetes-key.pem

```

## Par de chaves da conta de serviço (Service Account)
O Kubernetes Controller Manager aproveita o par de chaves para gerar e assinar os tokens das contas de serviço.
Esse processo está descrito na documentação oficial: 

https://kubernetes.io/docs/reference/access-authn-authz/authentication/#authentication-strategies

Gerar o certificados e chave privada para service-account:

```
# vim service-account-csr.json 

{
"CN": "service-accounts",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "Kubernetes",
      "OU": "Kubernetes RAIZ",
      "ST": "DF"
    }
  ]
}
```

```
# cfssl gencert  -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes   service-account-csr.json | cfssljson -bare service-account

2020/04/13 06:44:15 [INFO] generate received request
2020/04/13 06:44:15 [INFO] received CSR
2020/04/13 06:44:15 [INFO] generating key: rsa-2048
2020/04/13 06:44:15 [INFO] encoded CSR
2020/04/13 06:44:15 [INFO] signed certificate with serial number 718695701468244754069119792252125342592286659635
2020/04/13 06:44:15 [WARNING] This certificate lacks a "hosts" field. This makes it unsuitable for
websites. For more information see the Baseline Requirements for the Issuance and Management
of Publicly-Trusted Certificates, v.1.1.6, from the CA/Browser Forum (https://cabforum.org);
specifically, section 10.2.3 ("Information Requirements").

```
## Resultados esperados:

```
-rw-rw-r--. 1 vagrant vagrant 1415 Apr 13 06:44 service-account.pem
-rw-r--r--. 1 vagrant vagrant 1025 Apr 13 06:44 service-account.csr
-rw-------. 1 vagrant vagrant 1675 Apr 13 06:44 service-account-key.pem
 
```
# Distribuir os Certificados Client and Server

Copiar o certificado e a chave privada apropriada para cada instancia "worker"

```
[vagrant@admin-lb jsons]$ vim distribuir_certificados.sh 


#!/bin/bash

# Copiar o certificado e a chave privada apropriada para cada instancia "worker"
for instance in node1.k8s.multimasters.raiz node2.k8s.multimasters.raiz node3.k8s.multimasters.raiz; do
  scp -C ca.pem kube-proxy.pem kube-proxy-key.pem ${instance}-key.pem ${instance}.pem ${instance}:~/
done

#Copiar o certificado e a chave privada apropriada para cada instância de controller:
for instance in master1.k8s.multimasters.raiz master2.k8s.multimasters.raiz master3.k8s.multimasters.raiz; do
  scp -C ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem ${instance}:~/
done


```
```
 # sh distribuir_certificados.sh
```


