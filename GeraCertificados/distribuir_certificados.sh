#!/bin/bash

# Copiar o certificado e a chave privada apropriada para cada instancia "worker"
for instance in node.k8s.hard.com node2.k8s.hard.com node3.k8s.hard.com; do
  scp -C ca.pem kube-proxy.pem kube-proxy-key.pem ${instance}-key.pem ${instance}.pem root@${instance}:/home/vagrant/
  ssh root@${instance} chown vagrant:vagrant /home/vagrant/*

done

#Copiar o certificado e a chave privada apropriada para cada instância de controller:
for instance in master1 master2 master3; do
  scp  -C ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem  service-account-key.pem service-account.pem root@${instance}:/home/vagrant/
  ssh root@${instance} chown vagrant:vagrant /home/vagrant/*

done

