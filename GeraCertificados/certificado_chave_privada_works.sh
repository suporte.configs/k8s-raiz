#!/bin/bash

declare -A workers

workers[0,1]='node1.k8s.hard.com'
workers[0,2]='172.27.11.21'
workers[1,1]='node2.k8s.hard.com'
workers[1,2]='172.27.11.22'
workers[2,1]='node3.k8s.hard.com'
workers[2,2]='172.27.11.23'

for ((workerName=0; workerName <= 2; workerName++)); do
cat > ${workers[${workerName},1]}-csr.json <<EOF
{
  "CN": "system:node:${workers[${workerName},1]}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Brasilia",
      "O": "system:nodes",
      "OU": "Kubernetes RAIZ",
      "ST": "DF"
    }
  ]
}
EOF

INTERNAL_IP="${workers[${workerName},2]}"

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${workers[${workerName},1]},${INTERNAL_IP} \
  -profile=kubernetes \
  ${workers[${workerName},1]}-csr.json | cfssljson -bare ${workers[${workerName},1]}
done

